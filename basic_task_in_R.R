###############Basic task in R
library(tidyverse)
library(purrr) #provides the map()
library(data.table)#provides the fread()

# Task: Write a for loop that loops through all the file paths in the specdata folder
# and uses the function written in previous point to read the data from the .csv files.
# Create a table with columns that show: 
# a) ID nr 
# b) the nr of rows in each of the read in tables 
# c) The number of rows where atleast sulfate or nitrate data is not missing.

ReadPlusNames <- function(flnm) {# function to read in files and correct the file names
  fread(flnm) %>% 
    mutate(filename = flnm) %>% 
    mutate(name = substr(flnm, 12, nchar(filename) - 4)) %>% 
    select(-filename)
}


spec_data <-
  list.files(path = "./specdata/",
             pattern = "*.csv", 
             full.names = T) %>% 
  map_df(~ReadPlusNames(.)) %>%
  group_by(name) %>% 
  summarize(no_Rows = n(), not_NA = sum(!is.na(sulfate) | !is.na(nitrate)))# returns rows where at least sulfate or nitrate data is not missing. used the logical operator |

head(spec_data)

# A tibble: 6 x 3
# name  no_Rows not_NA
# <chr>   <int>  <int>
# 1 001      1461    122
# 2 002      3652   1051
# 3 003      2191    249
# 4 004      3652    479
# 5 005      2922    405
# 6 006      1826    229


# Using a for loop
library(readr)



list_files = list.files(path = "./specdata", 
                        pattern = "*.csv", 
                        full.names =  TRUE)

spec_data <- vector('list', length = length(list_files))# empty list to store the dataframes

data <-  data.frame(file_no = 1:length(list_files), # dataframe to store the outputs
                    num_rows = NA, 
                    num_rows_not_na = NA)

for (i in seq_along(list_files)) {
  spec_data[[i]] <- read_csv(list_files[[i]])
  data$num_rows[i] <- nrow(spec_data[[i]])
  data$num_rows_not_na[i] <- sum(!is.na(spec_data[[i]]$sulfate) | !is.na(spec_data[[i]]$nitrate))
}

head(data)